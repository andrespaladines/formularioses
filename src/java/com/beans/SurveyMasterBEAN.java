package com.beans;

import com.dao.ParticipantsDAO;
import com.dao.QOptionsDAO;
import com.dao.SurveyMasterDAO;
import com.entities.Question;
import com.entities.Section;
import com.utils.Tools;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author DiegoAndre
 */
@Named(value = "surveyMasterBEAN")
@SessionScoped
public class SurveyMasterBEAN implements Serializable {

    @Inject
    AccesBEAN accesBEAN;
    
    @EJB
    PropertiesBEAN prop;

    private List<Section> l_sections = null; ///////////Lista de secciones
    private int id_businessUnit = 1;
    private int id_activeSurvey = 0;
    private String mensajeVista = "";
    FacesMessage msg;
    private String s_tittle = "";
    private String s_message = "";
    private boolean saveAnswersState = false;
    private boolean ready = false;
    private int idParticipant = 123; //TODO: por pruebas
    private boolean isCharged = false;
    private String validateMesssage = "";
    private HashMap<String, String> l_hash;
    private HashMap<String, String[]> l_hashValues;
    private Date actual_date = null;
    private String name_client = "";
    private String unique_code = "";
    //--==============datos para imagenes y redireccionamiento
    private String image_name = "";
    private String image_path = "";
    private String redirect_url = "";
    
    
    private String ls_imgHeader = "";
    private String ls_imgMeditor = "";
    private String ls_note = "";

    public SurveyMasterBEAN() {
        actual_date = new Date();
        l_sections = new ArrayList();
    }

    public void setHashInformation(HashMap<String, String> pl_hash) {
        l_hash = pl_hash;
    }
    

    public HashMap<String, String[]> getL_hashValues() {
        return l_hashValues;
    }

    public int getId_activeSurvey() {
        return id_activeSurvey;
    }

    public void setId_activeSurvey(int id_activeSurvey) {
        this.id_activeSurvey = id_activeSurvey;
    }

    public void setL_hashValues(HashMap<String, String[]> l_hashValues) {
        this.l_hashValues = l_hashValues;
    }
    
    
    
    public boolean isMultipleResponse(String indice) {
        return "MR".equals(indice);
    }

    public boolean isMultiple(String indice) {
        return "M".equals(indice);
    }

    public boolean isBoolean(String indice) {
        return "SN".equals(indice);
    }

    public boolean isOpen(String indice) {
        return "A".equals(indice);
    }

    public boolean isMandatory(String ps_char) {
        boolean resp = false;
        if ("S".equals(ps_char) || "SI".equals(ps_char)) {
            resp = true;
        }
        return resp;
    }

    public String concatStrings(int index, String content, boolean mandatory,String tipo) {
        String asterisc = "";
        String varios = "";
        if("MR".equals(tipo)){
            varios = "<font size=\"3\" color=\"red\">(Puede escoger varios)</font>";
        }
        if (mandatory) {
            asterisc = " <strong>*</strong>";
        }
        return ("<strong>" + index + "</strong>" + " - " + content + "<strong><font size=\"5\" color=\"red\">" + asterisc + "</font></strong>"+varios);
    }

    public void getDataForPresentation(boolean answer) {
        Tools tool;
        try {
            tool = new Tools();
            image_path = tool.getValueForParameter("EXTERNAL_IMAGES");
            redirect_url = tool.getValueForParameter("REDIRECT_PAGE");
            if (answer) {
                image_name = tool.getValueForParameter("SUCCESS_IMAGE");
            } else {
                image_name = tool.getValueForParameter("ERROR_IMAGES");
            }
        } catch (Exception ex) {
            ex.printStackTrace(System.err);
        }
    }

    public StreamedContent getImage() {
        //getFullImagePath
        byte[] imagen;
        Tools tool = new Tools();
        FacesContext context = FacesContext.getCurrentInstance();
        if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
            return new DefaultStreamedContent();
        } else {
            imagen = tool.obtainLocalImageToArray(getFullImagePath());
        }
        return new DefaultStreamedContent(new ByteArrayInputStream(imagen));
    }

    public void RedirectPage() throws IOException {
        redirectOnError(redirect_url);
        //se mata la sesion para no retroceder
        accesBEAN.killSesion();
        isCharged = false; 
    }

    public void displayMessage() {
        getDataForPresentation(saveAnswersState);
        RequestContext context = RequestContext.getCurrentInstance();
        RequestContext.getCurrentInstance().update("mainForm:ExistMessage");
        context.execute("PF('advertenciaFinal').show();");
        
    }

    public String getFullImagePath() {
        return image_path + image_name;
    }

    public String getAllAnswers() {
        String page = "";
        int tmp_id_participante = 0;
        HashMap<String, String> ph_participantInfo = null;
        ParticipantsDAO partDAO = null;
        SurveyMasterDAO surveyMdao = null;
        boolean allowUser = false;
        try {
            surveyMdao = new SurveyMasterDAO();
            ph_participantInfo = accesBEAN.getL_hash();
            ph_participantInfo.put("cedula", accesBEAN.getParticipant_cc());
            ph_participantInfo.put("encuesta", String.valueOf(id_activeSurvey));
            allowUser = prop.getBoolean("veris.ipn.allowMultipleSubmits", false);
            if (!surveyMdao.allReadyGetThisSurvey(accesBEAN.getParticipant_cc(), id_activeSurvey,accesBEAN.getUrlDate()) /*|| allowUser*/) {
                partDAO = new ParticipantsDAO();
                tmp_id_participante = partDAO.saveParticipant(ph_participantInfo);
                saveAnswersState = putAllAnswersToBase(l_sections, tmp_id_participante,accesBEAN.getUrlDate(),accesBEAN.getCnmIdMessage());
                page = "FinalMessagePage.xhtml?faces-redirect=true";
            } else {
                showWarning("Su respuesta ya fue receptada.");
            }
        } catch (Exception ex) {
            ex.printStackTrace(System.err);
        }
        
        return page;
    }

    public boolean putAllAnswersToBase(List<Section> pl_sections, int idParticipant,String urlFecha,String idCnm) {
        boolean resp = false;
        QOptionsDAO qOptionsDAO = null;
        try {
            qOptionsDAO = new QOptionsDAO();
            resp = qOptionsDAO.saveAnswerToBase(pl_sections, idParticipant, id_businessUnit, s_tittle,urlFecha,idCnm);
        } catch (Exception ex) {
            ex.printStackTrace(System.err);
        }
        return resp;
    }

    public void redirectOnErrorN(int page) throws IOException {
        isCharged = false;
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        switch (page) {
            case 1:
                externalContext.redirect("AccessPage.xhtml?faces-redirect=true");
                break;
        }
    }

    public void redirectOnError(String page) throws IOException {
        isCharged = false;
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        externalContext.redirect(/*"AccessPage.xhtml?redirect=true"*/page);
    }

    public void showSimpleMessage(String message) {
        RequestContext context = RequestContext.getCurrentInstance();
        validateMesssage = message;
        context.execute("PF('advertenciaSS').show();");
    }

    public void showWarning(String message) {
        RequestContext context = RequestContext.getCurrentInstance();
        validateMesssage = message;
        context.execute("PF('advertenciaE').show();");
    }

    public String normalizeMessage(String messageDB) {
        String tmp = messageDB.replaceAll("#", "</br>");
        return tmp;
    }

    public void getAllSurveyInformation() throws IOException {
        boolean allowUser = false;
        if (!isCharged) {
            redirectOnError("AccessPage.xhtml?faces-redirect=true");
        } else {
            SurveyMasterDAO surveyMdao = null;
            try {
                surveyMdao = new SurveyMasterDAO();
                if(id_activeSurvey <= 0){
                   id_activeSurvey = surveyMdao.getActiveSurveyFromBusinessUnit(id_businessUnit);
                }
                if (id_activeSurvey > 0) {
                    //se valida que no tenga encuestas ya contestadas activas con su cedula
                    allowUser = prop.getBoolean("veris.ipn.allowMultipleSubmits", false);
                    if (!surveyMdao.allReadyGetThisSurvey(accesBEAN.getParticipant_cc(), id_activeSurvey,accesBEAN.getUrlDate()) /*|| allowUser*/) {
                        //obtener mensaje de bienvenida
                        surveyMdao.getActiveSurveyById2(id_activeSurvey);
                        s_tittle = surveyMdao.getLs_tittle();
                        s_message = surveyMdao.getLs_message();
                        ls_imgHeader = surveyMdao.getLs_imgHeader();
                        ls_imgMeditor = surveyMdao.getLs_imgMeditor();
                        ls_note = surveyMdao.getLs_note();
                        //se setean datos adicionales de cabecera
                        //2018-11-11 DAV - IPN veris
                            name_client = l_hash.get("nombre");
                            unique_code = l_hash.get("ci");
                        //2019-01-29 DAV - se retira popup por IPN
                        //showWarning(normalizeMessage(s_message));
                        //2018-11-11 DAV - IPN veris
                        ready = true;
                        l_sections = surveyMdao.getAllSections(id_activeSurvey,l_hash,l_hashValues);
                        
                    } else {
                        showWarning("Usted ya cuenta con encuesta respondida.");
                    }
                } else {
                    showWarning("No se encontraron encuestas activas: ");
                }
            } catch (Exception ex) {
                ex.printStackTrace(System.err);
            } finally {
                surveyMdao = null;
            }
        }
    }

    //l_sections
    public boolean allMandatoryAnswared() {
        boolean its_ok = true;
        for (Section sec : l_sections) {
            for (Question quest : sec.getSl_questions()) {
                if (quest.isP_mandatory()) {
                    //se evalua las respuestas
                    if ((quest.getP_idOptionAnswer() == null) && (quest.getP_openAnswer().length() <= 0)
                            && (quest.getP_ListIdOptionAnswer().length <= 0)) {
                        its_ok = false;
                        System.out.println("Preguntas faltantes : nª" + quest.getP_questionId());
                        break;
                    }
                }
            }
        }
        return its_ok;
    }

    public String finalMessage() {
        String secciones = "";
        String page = "";
        int count = 0;
        if (l_sections.size() > 0) {
            for (int i = 0; i < l_sections.size(); i++) {
                if (count == 0) {
                    secciones = l_sections.get(i).getS_title();
                    count++;
                } else {
                    secciones = secciones + ", </br> " + l_sections.get(i).getS_title();
                    count++;
                }
            }
        }

        if (allMandatoryAnswared()) {
            ready = false;
            page = getAllAnswers();
        } else {
            showSimpleMessage("Por favor asegúrese  de responder todas las preguntas Obligatorias </br>"
                    + "marcadas con un asterisco(*) en todas las secciones de la encuesta:</br></br>"
                    + "<strong>Lista de Secciones:</strong></br> "
                    + secciones + " ");
        }
        return page;
    }
    
    public boolean isCallbackNecessary(boolean a,boolean b){
        if(a && b){
            return true;
        }else{
            return false;
        }
    }

    public void mensajeVista(String mensaje, int severidad) {
        //1.- info ,2.- warning, 3.-error
        msg = new FacesMessage(mensaje, mensaje);
        switch (severidad) {
            case 1:
                msg.setSeverity(FacesMessage.SEVERITY_INFO);
                break;
            case 2:
                msg.setSeverity(FacesMessage.SEVERITY_WARN);
                break;
            case 3:
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                break;
            default:
                msg.setSeverity(FacesMessage.SEVERITY_FATAL);
                break;
        }
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public boolean isReady() {
        return ready;
    }

    public void setReady(boolean ready) {
        this.ready = ready;
    }
    

    public String getValidateMesssage() {
        return validateMesssage;
    }

    public void setValidateMesssage(String validateMesssage) {
        this.validateMesssage = validateMesssage;
    }

    public String getS_tittle() {
        return s_tittle;
    }

    public void setS_tittle(String s_tittle) {
        this.s_tittle = s_tittle;
    }

    public String getS_message() {
        return s_message;
    }

    public void setS_message(String s_message) {
        this.s_message = s_message;
    }

    public List<Section> getL_sections() {
        return l_sections;
    }

    public void setL_sections(List<Section> l_sections) {
        this.l_sections = l_sections;
    }

    public int getId_businessUnit() {
        return id_businessUnit;
    }

    public String getActual_date() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        return dateFormat.format(actual_date);
    }

    public void setActual_date(Date actual_date) {
        this.actual_date = actual_date;
    }

    public String getName_client() {
        return name_client;
    }

    public void setName_client(String name_client) {
        this.name_client = name_client;
    }

    public String getUnique_code() {
        return unique_code;
    }

    public void setUnique_code(String unique_code) {
        this.unique_code = unique_code;
    }

    public void setId_businessUnit(int id_businessUnit) {
        this.id_businessUnit = id_businessUnit;
    }

    public String getMensajeVista() {
        return mensajeVista;
    }

    public void setMensajeVista(String mensajeVista) {
        this.mensajeVista = mensajeVista;
    }

    public int getIdParticipant() {
        return idParticipant;
    }

    public void setIdParticipant(int idParticipant) {
        this.idParticipant = idParticipant;
    }

    public boolean isSaveAnswersState() {
        return saveAnswersState;
    }

    public void setSaveAnswersState(boolean saveAnswersState) {
        this.saveAnswersState = saveAnswersState;
    }

    public boolean isIsCharged() {
        return isCharged;
    }

    public void setIsCharged(boolean isCharged) {
        this.isCharged = isCharged;
    }

    public String getLs_imgHeader() {
        return ls_imgHeader;
    }

    public void setLs_imgHeader(String ls_imgHeader) {
        this.ls_imgHeader = ls_imgHeader;
    }

    public String getLs_imgMeditor() {
        return ls_imgMeditor;
    }

    public void setLs_imgMeditor(String ls_imgMeditor) {
        this.ls_imgMeditor = ls_imgMeditor;
    }

    public String getLs_note() {
        return ls_note;
    }

    public void setLs_note(String ls_note) {
        this.ls_note = ls_note;
    }

}
