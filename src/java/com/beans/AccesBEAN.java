package com.beans;

import com.bo.AccessBO;
import com.dao.AccessDAO;
import com.entities.BusinessUnit;
import com.utils.Consumer;
import com.utils.Tools;
import java.io.IOException;
import java.io.Serializable;
import java.net.SocketTimeoutException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Administrador
 */
@Named(value = "accesBEAN")
@SessionScoped
public class AccesBEAN implements Serializable{
    
    @Inject
    SurveyMasterBEAN SurveyBEAN;
    
    @EJB
    PropertiesBEAN prop;
    
    private String inicales_UN = "";
    private int unidad_negocio = 0;
    private String participant_cc = "";
    private String urlDate = "";
    private String cnmIdMessage = "";
    private List<BusinessUnit> ll_businessUnits = null;
    private HashMap<String,String[]> has = null;
    private String validateMandatoryField ="*El numero de cedula es obligatorio";
    private String validateMandatoryFieldC = "*La ciudad es obligatoria";
    FacesMessage msg;
    private boolean showGif = false;
    private HashMap<String,String> l_hash = null;
    private HashMap<String,String[]> l_hashReplacements = null;
    private boolean isIpn = true;
    
    public AccesBEAN(){
        ll_businessUnits = new ArrayList();
    }
    
    public void getAllLocations() throws SQLException, IOException{
        participant_cc = "";
        urlDate = "";
        String page = "";
        AccessDAO accessDAO = new AccessDAO();
        isIpn = true;
        String[] arrIdSurvey = null;
        //15-10-2018 DAV - se agrega para capturar parametros de sesion
        FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext ec = context.getExternalContext();
        HttpServletRequest req = (HttpServletRequest)ec.getRequest();
        has = new HashMap();
        has.putAll(req.getParameterMap());
        SurveyBEAN.setId_activeSurvey(0);
        for (String name: has.keySet()){
            String key =name;  
            String[] arr = (String[])has.get(key);
            if("ci".equals(key)){
                participant_cc = arr[0];
                inicales_UN=prop.getString("veris.ipn.defaultBussinesUnit", "1-SIM");
                //25-02-2019 DAV - se agrega recepcion de fecha por parametros
                String[] arrFecha = (String[])has.get("fecha");
                if(arrFecha != null && arrFecha[0] != null){
                   urlDate = arrFecha[0];
                }
                //08-07-2019 DAV - se agrega id de campania activa por url
                arrIdSurvey = (String[])has.get("survey");
                if(arrIdSurvey != null && arrIdSurvey[0] != null){
                   SurveyBEAN.setId_activeSurvey(Integer.parseInt(arrIdSurvey[0]));
                   isIpn = false;
                }
                
                //19-11-2019 DAV - se agrega id de mensaje que llega por url
                //cnmIdMessage
                String[] arrCnmId = (String[])has.get("identifierCNM");
                if(arrCnmId != null && arrCnmId[0] != null){
                    cnmIdMessage = arrCnmId[0];
                }
                
                validateStartSurvey();
            }
        } 
        //----------
        ll_businessUnits = accessDAO.getAllBusinessUnits();
        
        //return page;
    }
    
    public String concatValues(int id_bu,String iniciales){
        return (id_bu+"-"+iniciales);
    }
    
    public void killSesion(){
        FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext ec = context.getExternalContext();
        HttpSession htp = (HttpSession) ec.getSession(false);
        htp.invalidate();
    }
    
    public boolean proccessIpnCall(String participant_cc,String urlDate,String connect_TO,String read_TO) throws Exception{
        boolean troubled = false;
        String ls_json = null;
        Consumer consum = null;
        Tools tool = null;
        try{
            consum = new Consumer(prop);
            tool = new Tools();
            ls_json = consum.getJsonIpnInformation(participant_cc,urlDate,connect_TO,read_TO);
            if(ls_json != null){
                l_hash = tool.getIPNvalues(ls_json);
                if(l_hash != null){
                    String error = l_hash.get("error_WS");
                    String nombre = l_hash.get("nombre");
                    if(error == null && nombre != null){
                        l_hashReplacements = tool.getIPNparameters(ls_json);
                    }else{
                        troubled = true;
                        mensajeVista("No se pudo procesar su usuario, por favor inténtelo mas tarde.",2);   
                    }
                }else{
                    troubled = true;
                    throw new SocketTimeoutException("Error");
                }
            }else{
                troubled = true;
                mensajeVista("Estimado cliente su requerimiento no pudo ser procesado.",2); 
            }
        }catch(SocketTimeoutException tex){
            troubled = true;
            throw tex;
        }catch(Exception ex){
            ex.printStackTrace();
            troubled = true;
            throw ex;
        }
        
        return troubled;
    }
    
    
    public void validateStartSurvey(){
        AccessBO accessBO = null;
        Tools tool = null;
        String tue_id_bu = "";
        String[] data = null;
        String connect_TO ="";
        String read_TO ="";
        String page = "";
        boolean ipnStatus = false;
        try{
          l_hash = new HashMap();
          l_hashReplacements = null;
          data = inicales_UN.split("-");
          tue_id_bu = data[0];

          accessBO = new AccessBO();
          tool = new Tools();
          if(accessBO.isValidCC(participant_cc)){
            connect_TO = tool.getValueForParameter("CONNECT_TIMEOUT");
            read_TO = tool.getValueForParameter("READ_TIMEOUT");
            if(isIpn){ //debe consumir servicio
                ipnStatus = proccessIpnCall(participant_cc,urlDate,connect_TO,read_TO);
            }
            SurveyBEAN.setIsCharged(true);
            l_hash.put("ci", participant_cc);
            l_hash.put("fecha", urlDate);
            SurveyBEAN.setHashInformation(l_hash);
            SurveyBEAN.setL_hashValues(l_hashReplacements);
            SurveyBEAN.setId_businessUnit(Integer.parseInt(tue_id_bu));
            page = "SurveyMasterPage.xhtml?faces-redirect=true";
            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
            externalContext.redirect(page);
            
          }else{
             mensajeVista("Cedula incorrecta!",4); 
          }
        }catch(SocketTimeoutException tex){
            mensajeVista("Lo sentimos, por el momento el servicio no está disponible, inténtelo más tarde.",2);
        }catch(Exception ex){
            mensajeVista("Error general en la aplicacion.",2);
            ex.printStackTrace(System.out);
        }finally{
            showGif = false;
        }

    }
    
    public void mensajeVista(String mensaje, int severidad) {
        //1.- info ,2.- warning, 3.-error
        msg = new FacesMessage(mensaje, mensaje);
        switch (severidad) {
            case 1:
                msg.setSeverity(FacesMessage.SEVERITY_INFO);
                break;
            case 2:
                msg.setSeverity(FacesMessage.SEVERITY_WARN);
                break;
            case 3:
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                break;
            default:
                msg.setSeverity(FacesMessage.SEVERITY_FATAL);
                break;
        }
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public HashMap<String, String> getL_hash() {
        return l_hash;
    }

    public void setL_hash(HashMap<String, String> l_hash) {
        this.l_hash = l_hash;
    }

    public List<BusinessUnit> getLl_businessUnits() {
        return ll_businessUnits;
    }

    public void setLl_businessUnits(List<BusinessUnit> ll_businessUnits) {
        this.ll_businessUnits = ll_businessUnits;
    }

    public String getInicales_UN() {
        return inicales_UN;
    }

    public void setInicales_UN(String inicales_UN) {
        this.inicales_UN = inicales_UN;
    }

    public int getUnidad_negocio() {
        return unidad_negocio;
    }

    public void setUnidad_negocio(int unidad_negocio) {
        this.unidad_negocio = unidad_negocio;
    }

    public String getParticipant_cc() {
        return participant_cc;
    }

    public void setParticipant_cc(String participant_cc) {
        this.participant_cc = participant_cc;
    }

    public String getUrlDate() {
        return urlDate;
    }

    public void setUrlDate(String urlDate) {
        this.urlDate = urlDate;
    }
    
    public String getValidateMandatoryField() {
        return validateMandatoryField;
    }

    public void setValidateMandatoryField(String validateMandatoryField) {
        this.validateMandatoryField = validateMandatoryField;
    }

    public String getValidateMandatoryFieldC() {
        return validateMandatoryFieldC;
    }

    public void setValidateMandatoryFieldC(String validateMandatoryFieldC) {
        this.validateMandatoryFieldC = validateMandatoryFieldC;
    }

    public boolean isShowGif() {
        return showGif;
    }

    public void setShowGif(boolean showGif) {
        this.showGif = showGif;
    }

    public String getCnmIdMessage() {
        return cnmIdMessage;
    }

    public void setCnmIdMessage(String cnmIdMessage) {
        this.cnmIdMessage = cnmIdMessage;
    }
    
    
    
    
}
