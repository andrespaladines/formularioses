package com.beans;

import java.io.File;
import java.net.URI;
import java.net.URL;
import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.reloading.FileChangedReloadingStrategy;

/**
 *
 * @author Administrador
 */
@Singleton(mappedName = "PropertiesBEAN")
public class PropertiesBEAN {
    private PropertiesConfiguration config;
    private URL url;
    
    @PostConstruct
    public void cargarProperties(){
        try { 
            File archivoProperties = new File("/ses/config/ses.properties");
            URI uri = archivoProperties.toURI();
            url = uri.toURL();
            config = new PropertiesConfiguration(url);
            //----------
            FileChangedReloadingStrategy fileChangedReloadingStrategy = new FileChangedReloadingStrategy();
            fileChangedReloadingStrategy.setRefreshDelay(Integer.parseInt("5000")); //tiempo de refresco en milisegundos
            config.setReloadingStrategy(fileChangedReloadingStrategy);
        } catch (Exception e) {
            // TODO
            e.printStackTrace();
        }
    }
    
    public String obtenerPropiedad(String clave){
        return config.getString(clave,"EOF");
    }
    
    public boolean obtenerEstadoPropiedad(String clave){
        return config.getBoolean(clave, false);
    }
    
    public int getInt(String valor,int porDefecto){
        return config.getInt(valor,porDefecto);
    }
    
    public String getString(String valor,String porDefecto){
        return config.getString(valor,porDefecto);
    }
    
    public boolean getBoolean(String valor,boolean porDefecto){
        return config.getBoolean(valor, porDefecto);
    }
}
