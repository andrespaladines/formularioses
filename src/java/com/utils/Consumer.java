package com.utils;

import com.beans.PropertiesBEAN;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
/**
 *
 * @author Administrador
 */
public class Consumer {
    
    static String verisEndpoint = "http://localhost:8081/RedisCoreCNM-war/CoreWSRestHttpPort/GetFromMemory";
    private PropertiesBEAN propsBeans;
    private DefaultHttpClient defaultClient = null;
    
    public Consumer(PropertiesBEAN p_propsBeans){
        propsBeans = p_propsBeans;
    }
    
    //2018-11-10 DAV - IPN veris
    public String getJsonIpnInformation(String participantId,String aditionalValidator,String time_out,String read_time_out) throws SocketTimeoutException{
        
        HttpPost request = null;
        HttpResponse response = null;
        InputStream reader = null;
        StringBuilder finalEntity = null;
        String entityResponse = "";
        ArrayList<NameValuePair> postParameters;
        try{
            defaultClient = new DefaultHttpClient();
            verisEndpoint = propsBeans.obtenerPropiedad("veris.ipn.endPoint");
            request = new HttpPost(verisEndpoint);
            finalEntity = new StringBuilder();
            //se agregan timeouts
            RequestConfig requestConfig = RequestConfig.custom()
            .setSocketTimeout(Integer.parseInt(time_out))
            .setConnectTimeout(Integer.parseInt(read_time_out))
            .setConnectionRequestTimeout(Integer.parseInt(read_time_out))
            .build();
            request.setConfig(requestConfig);
            postParameters = new ArrayList<NameValuePair>();
            postParameters.add(new BasicNameValuePair("identificacion", participantId));
            System.out.println("identificacion:"+participantId);
            //VERIS IPN - solo para consulta de datos adicionales
            if(aditionalValidator != null && aditionalValidator.length()> 0){
                postParameters.add(new BasicNameValuePair("fecha", aditionalValidator));
                System.out.println("fecha:"+aditionalValidator);
            }
                
            request.setEntity(new UrlEncodedFormEntity(postParameters, "UTF-8"));
            response = defaultClient.execute(request);
            StatusLine statusLine = response.getStatusLine();
            //print result
            HttpEntity enti = response.getEntity();
            reader = enti.getContent();
            finalEntity.append(convertStreamToString(reader));
            
            entityResponse = finalEntity.toString();
        }catch(SocketTimeoutException tex){    
            entityResponse = "TIMEOUT_WS";
            throw new SocketTimeoutException(tex.getMessage());
        }catch(Exception ex){
            entityResponse = "GENERAL ERROR_WS "+ex.getMessage();
            ex.printStackTrace();
        }finally{
            if(defaultClient != null){
                defaultClient.getConnectionManager().shutdown();
            }
        }
        return entityResponse;
    }
    
    private static String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
    
    @Deprecated
    public String getJsonInformation(String initials,String participantId,String time_out,String read_time_out) throws SocketTimeoutException{
        String response = "";
        String tmp = "";
        String tmp_request = ConstantsSES.REQUEST_WS_CONSULT;
        Tools tool;
        String tmp_url = "";
        int tmp_to_connect = 10000;
        int tmp_to_read = 10000;
        try{
            tool = new Tools();
            tmp_request = tmp_request.replace("@1@", initials);
            tmp_request = tmp_request.replace("@2@", participantId);
            //URL url = new URL(ConstantsSES.URL_WS); // se cambia para dinamismo de consumo WS
            tmp_url = tool.getValueForParameter("WS_URL");
            URL url = new URL(tmp_url);
            URLConnection con = url.openConnection();
            HttpURLConnection httpcon = (HttpURLConnection)con;
            if(time_out.length()>0 && read_time_out.length()>0){
                tmp_to_connect = Integer.parseInt(time_out);
                tmp_to_read = Integer.parseInt(read_time_out);
            }
            
            httpcon.setConnectTimeout(tmp_to_connect);
            httpcon.setReadTimeout(tmp_to_read);
            ByteArrayOutputStream bout = new ByteArrayOutputStream();  
            byte[] buffer = new byte[tmp_request.length()];
            buffer = tmp_request.getBytes();
            bout.write(buffer);
            byte[] b = bout.toByteArray();
            String soapAction = "";
            //parametros del request
            httpcon.setRequestProperty("Content-Length",String.valueOf(b.length));
            httpcon.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
            httpcon.setRequestProperty("SOAPAction",soapAction);
            httpcon.setRequestMethod("POST");
            httpcon.setDoInput(true);
            httpcon.setDoOutput(true);
            OutputStream out = httpcon.getOutputStream();
            out.write(b);
            out.close();
            //leer la respuesta
            InputStreamReader is = new InputStreamReader(httpcon.getInputStream());
            BufferedReader br = new BufferedReader(is);
            //paso la respuesta a string
            while((response = br.readLine())!= null){
                tmp = tmp + response+"\n";
            }
        }catch(SocketTimeoutException tex){    
            tmp = "TIMEOUT_WS";
            throw new SocketTimeoutException(tex.getMessage());
        }catch(Exception ex){
            tmp = "GENERAL ERROR_WS "+ex.getMessage();
        }
        return tmp;
    }
    
}
