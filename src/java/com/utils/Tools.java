package com.utils;

import com.dao.Conexion;
import static com.utils.ConstantsSES.BASE_SCHEMA;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.net.SocketTimeoutException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import javax.imageio.ImageIO;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author DiegoAndre
 */
public class Tools{
    
    public byte[] obtainLocalImageToArray(String urlImage){
        byte[] foto = null;
        try{
            File imgPath = new File(urlImage);
            BufferedImage buf_image = ImageIO.read(imgPath);
            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            ImageIO.write(buf_image, "jpg", bout);
            foto = bout.toByteArray();
        }catch(Exception ex){
            ex.printStackTrace(System.err);
        }
        return foto;
    }
    
    public String getValueForParameter(String id_parameter) throws SQLException {
        String number_winners = "";
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet ResultSet = null;
        try {
            connection = (new Conexion().getConexion());
            preparedStatement = connection.prepareStatement(ConstantsSES.QUERY_OBTAIN_PARAMETER_VALUE);
            preparedStatement.setString(1, id_parameter);
            ResultSet = preparedStatement.executeQuery();
            while (ResultSet.next()) {
                number_winners = ((ResultSet.getString("valor")));
            }
        } catch (Exception ex) {
            ex.printStackTrace(System.err);
        } finally {
            if (connection != null) {
                connection.close();
            }
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
        return number_winners;
    }
    
    public String getValueFromRequest(String text,String token){
        String message = "";
        int indice=0;
        int ind1,ind2;
        int ex=0;
        if(text.indexOf("<"+token+">")!=-1){
            ex=token.length(); //6
            indice = text.indexOf(token);
            ind2=text.indexOf(token, indice+5);
            message=text.substring(indice+1+ex, ind2-2);
        }else{
            message= "*No existe datos*";
        }
        return message;
    }
    
    public int getNextId(String ps_table_name) throws SQLException{
        int ln_seq = 0;
        Connection connection = null;
        CallableStatement cs = null;
        try{
            connection = (new Conexion().getConexion());
            cs = connection.prepareCall("{call "+BASE_SCHEMA+"ses_getNextId(?,?)}");
            cs.setString(1,ps_table_name);
            cs.registerOutParameter(2, java.sql.Types.INTEGER);
            cs.execute();
            ln_seq = cs.getInt(2);
            
        }catch(Exception ex){
            ex.printStackTrace(System.err);
        }finally{
            if(connection != null){
                connection.close();
            }
            if(cs != null){
               cs.close();
            }
        }
        return ln_seq;
    }
   
         public int getNextIdParticipant() throws SQLException{
        int ln_seq = 0;
        Connection connection = null;
        CallableStatement cs = null;
        String SqlNext = "select max(id_participante)+1 id_participante from ses.ses_participante";
        PreparedStatement pst;
        ResultSet resultSet2=null;
        int idParticipante = 0;
        try{
            connection = (new Conexion().getConexion());
            pst = connection.prepareStatement(SqlNext);
            resultSet2=pst.executeQuery();
            while (resultSet2.next()) {
            idParticipante = resultSet2.getInt("id_participante");
        }
        System.out.println("id_participante :" + idParticipante);
            
        }catch(Exception ex){
            ex.printStackTrace(System.err);
        }finally{
            if(connection != null){
                connection.close();
            }
            if(cs != null){
               cs.close();
            }
        }
        return idParticipante;
    }
    
     public int getNextIdResponse   () throws SQLException{
        int ln_seq = 0;
        Connection connection = null;
        CallableStatement cs = null;
        String SqlNext = "select max(id_respuesta)+1 id_respuesta from ses.ses_respuesta ";
        PreparedStatement pst;
        ResultSet resultSet2=null;
        int idRespuesta = 0;
        try{
            connection = (new Conexion().getConexion());
            pst = connection.prepareStatement(SqlNext);
            resultSet2 = pst.executeQuery();
            while (resultSet2.next()) {
            idRespuesta = resultSet2.getInt("id_respuesta");
        }
        System.out.println("ID_del idRespuesta :" + idRespuesta);
            
        }catch(Exception ex){
            ex.printStackTrace(System.err);
        }finally{
            if(connection != null){
                connection.close();
            }
            if(cs != null){
               cs.close();
            }
        }
        return idRespuesta;
    }
    
    @Deprecated
    public HashMap<String,String> getUserValues(String l_json) throws SocketTimeoutException{
        HashMap<String,String> l_values = null;
        JSONObject xmlJSONObj = null;
        JSONObject resultJson = null;
        JSONArray codes = null;
        JSONObject one_code = null;
        String status = "";
        try{
            //xmlJSONObj = XML.toJSONObject(l_json);
            /*xmlJSONObj = new JSONObject(l_json);
            status = xmlJSONObj.getString("status");
            if("OK".equals(status)){
                resultJson = xmlJSONObj.getJSONObject("Result");
                if(resultJson != null){
                    l_values = new HashMap();
                    l_values.put("cliente", resultJson.getString("cliente"));
                    codes = resultJson.getJSONArray("Cuentas");
                    if(codes !=null){
                        one_code = codes.getJSONObject(0);
                        if(one_code != null){
                            l_values.put("codigoUnico", one_code.getString("codigoUnico"));
                            l_values.put("medidor", one_code.getString("medidor"));
                            l_values.put("canton", one_code.getString("canton"));
                            l_values.put("calle_secundaria", one_code.getString("calle_secundaria"));
                            l_values.put("estado", one_code.getString("estado"));
                            return l_values;
                        }
                        return null;
                    }
                    return null;
                }
                return null;
            }*/
        }catch(Exception ex){
            throw new SocketTimeoutException("Error");
        }
        return l_values;
    }
    
    public HashMap<String,String> getIPNvalues(String l_json) throws SocketTimeoutException{
        HashMap<String,String> l_values = null;
        JSONObject xmlJSONObj = null;
        try{
            System.out.println(l_json);
            l_values = new HashMap();
            xmlJSONObj = stringToJson(l_json.trim());
            for(Object key : xmlJSONObj.keySet()){
                String keyStr = (String)key;
                if(!"listHashValues".equals(keyStr)){
                    String value = (String)xmlJSONObj.get(keyStr);
                    l_values.put(keyStr,value);
                }
            }
            
        }catch(Exception ex){
            ex.printStackTrace();
            throw new SocketTimeoutException("Error");
        }
        return l_values;
    }
    
    
    public HashMap<String,String[]> getIPNparameters(String l_json) throws SocketTimeoutException{
        HashMap<String,String[]> l_values = null;
        JSONObject xmlJSONObj = null;
        JSONArray jSONArray = null;
        try{
            l_values = new HashMap();
            xmlJSONObj = stringToJson(l_json.trim());
            
            jSONArray = (JSONArray)xmlJSONObj.get("listHashValues");
            if(jSONArray != null){
               Iterator objIter = jSONArray.iterator();
                while (objIter.hasNext()) {
                    JSONObject ob = (JSONObject)objIter.next();
                    String[] arrayData = new String[2];
                    arrayData[0] = (String)ob.get("value");
                    Long tmpData = (Long)ob.get("codigo");
                    arrayData[1] = String.valueOf(tmpData);
                    l_values.put((String)ob.get("key"),arrayData);
                } 
            }

        }catch(Exception ex){
            ex.printStackTrace();
            throw new SocketTimeoutException("Error");
        }
        return l_values;
    }
    
    public JSONObject stringToJson(String JsonString){
        JSONObject json = null;
        JSONParser parser = null;
        try{
           parser = new JSONParser();
           //propsBean.logger.debug("Se transforma a json el siguiente objeto :"+JsonString);
           json = (JSONObject) parser.parse(JsonString);
        }catch(ParseException ex){
            
        }     
        return json;
    }
  
}
