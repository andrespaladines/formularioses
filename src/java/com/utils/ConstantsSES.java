package com.utils;

/**
 *
 * @author DiegoAndre
 */
public abstract class ConstantsSES {
    
    public static String BASE_SCHEMA = "";
    public static String JNDI = "jdbc/ses";
    
    public static String QUERY_VIEW_SECTIONS = "select * \n" +
                                                "from "+BASE_SCHEMA+"ses_seccion s \n" +
                                                "where  s.id_encuesta = ? \n" +
                                                "and s.estado = 'A' order by nro_seccion";
    
    public static String QUERY_VIEW_QUESTIONS ="select * \n" +
                                                "from "+BASE_SCHEMA+"ses_pregunta p \n" +
                                                "where p.id_seccion = ? \n" +
                                                "and p.estado = 'A'";
    
    public static String QUERY_VIEW_OPTIONS = "select *\n" +
                                                "from "+BASE_SCHEMA+"ses_opciones \n" +
                                                "where id_pregunta = ? \n" +
                                                "and estado = 'A' \n" +
                                                "order by orden;";
    
    public static String QUERY_VIEW_RULES = "select *\n" +
                                                "from "+BASE_SCHEMA+"ses_regla_pregunta \n" +
                                                "where id_pregunta = ? \n" +
                                                "and estado = 'A';";
    
    public static String QUERY_VIEW_SURVEYS = "select * from\n" +
                                                "(SELECT * FROM "+BASE_SCHEMA+"ses_encuesta e \n" +
                                                "WHERE e.estado = 'A' \n" +
                                                "AND ID_UNIDAD_NEGOCIO = ? \n" +
                                                "AND NOW() between FECHA_INICIO AND FECHA_FIN \n" +
                                                "order by id_encuesta)t limit 1"; 
    
        public static String QUERY_VIEW_SURVEYS1 = "select * from\n" +
                                                "(SELECT * FROM "+BASE_SCHEMA+"ses_encuesta e \n" +
                                                "WHERE e.estado = 'A' \n" +
                                                "AND ID_ENCUESTA = ? \n" +
                                                "AND NOW() between FECHA_INICIO AND FECHA_FIN \n" +
                                                "order by id_encuesta)t limit 1"; 
    
    
    public static String QUERY_INSERT_ANSWERS = "insert into "+BASE_SCHEMA+"ses_respuesta(id_respuesta,id_participante,id_pregunta,id_opcion,respuesta_abierta,codigo,data_adicional_1,data_adicional_2)values(?,?,?,?,?,?,?,?)";
    //2018-11-11 DAV - IPN veris
    public static String QUERY_INSERT_PARTICIPANTS = "INSERT INTO "+BASE_SCHEMA+"ses_participante(id_participante,id_encuesta,cedula_ruc,codigo_unico,nombre_completo,fecha_registra_encuesta,estado,observacion,dato_adicional,dato_adicional_2)\n" +
                                                        "VALUES(?,?,?,?,?,now(),?,?,?,?)";
    
    public static String QUERY_OBTAIN_PARAMETER_VALUE="SELECT valor FROM "+BASE_SCHEMA+"ses_parametro where id_parametro = ?";
    
    public static String QUERY_SELECT_BU = "SELECT * FROM "+BASE_SCHEMA+"ses_unidad_negocio where estado = 'A' order by nombre";
    
    public static String QUERY_ALLREADY_SURVEY = "select count(*) from "+BASE_SCHEMA+"ses_participante where id_encuesta = ? and cedula_ruc = ? and dato_adicional_2 = ?";
    
    public static String REQUEST_WS_CONSULT = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ser=\"http://service.cnel.gob.ec/\">\n" +
                                                "   <soapenv:Header/>\n" +
                                                "   <soapenv:Body>\n" +
                                                "      <ser:getClientInfoByCC>\n" +
                                                "         <!--Optional:-->\n" +
                                                "         <arg0>@1@</arg0>\n" +
                                                "         <!--Optional:-->\n" +
                                                "         <arg1>@2@</arg1>\n" +
                                                "      </ser:getClientInfoByCC>\n" +
                                                "   </soapenv:Body>\n" +
                                                "</soapenv:Envelope>";
    
    public static String QUERY_RESPONSE_EXIST = "SELECT ifnull(id_consolidado,0) id_consolidado,count(*) cantidad,ifnull(cantidad_opcion,0) id_actual "
                                                                + "FROM "+BASE_SCHEMA+"ses_consolidado where id_UNIDAD_NEGOCIO = ? and id_encuesta = ? \n" +
                                                                    "and nombre_encuesta = ? and id_pregunta = ? and id_opcion = ?";
    
    public static String UPDATE_INCOMING_RESPONSE = "UPDATE "+BASE_SCHEMA+"ses_consolidado SET CANTIDAD_OPCION = ? WHERE ID_CONSOLIDADO = ?";
    
    public static String INSERT_INCOMING_RESPONSE = "INSERT INTO "+BASE_SCHEMA+"ses_consolidado values(?,?,?,?,?,?,?,?,?,?,?,?)";
    
    public static String RESPONSE_JSON_TEST = "{\n" +
                                                "	\"status\": \"OK\",\n" +
                                                "	\"Result\": {\n" +
                                                "		\"cliente\": \"ARAMBULO VITORES DIEGO ANDRE\",\n" +
                                                "		\"cedula\": \"0929800399\",\n" +
                                                "		\"Cuentas\": [{\n" +
                                                "			\"codigoUnico\": \"0401537695\",\n" +
                                                "			\"medidor\": \"1000170956\",\n" +
                                                "			\"canton\": \"Guayaquil\",\n" +
                                                "			\"recinto\": \"Febres Cordero\",\n" +
                                                "			\"ruta\": \"6290531357\",\n" +
                                                "			\"calle_secundaria\": \"24VA.CALLE\",\n" +
                                                "			\"referencia\": \"\",\n" +
                                                "			\"estado\": \"0\"\n" +
                                                "		}]\n" +
                                                "	}\n" +
                                                "}";
    //2018-11-11 DAV - IPN veris
    public static String RESPONSE_JSON_TEST_IPN = "{\n" +
                                                    "   \"areas\": \"CONSULTAS|IMAGENES|CAJAS|LABORATORIOS\",\n" +
                                                    "   \"nombre\": \"ZAMBRANO DELGADO ARIANA VALENTINA\",\n" +
                                                    "   \"idIpnSensorEvento\": \"174659,174660\",\n" +
                                                    "   \"listHashValues\":    [\n" +
                                                    "            {\n" +
                                                    "         \"key\": \"CAJAS\",\n" +
                                                    "         \"value\": \"5\"\n" +
                                                    "         \"codigo\": 1231\n" +
                                                    "      },\n" +
                                                    "            {\n" +
                                                    "         \"key\": \"LABORATORIOS\",\n" +
                                                    "         \"value\": \"RAYOS X\"\n" +
                                                    "         \"codigo\": 1232\n" +
                                                    "      },\n" +
                                                    "            {\n" +
                                                    "         \"key\": \"CIUDAD\",\n" +
                                                    "         \"value\": \"GUAYAQUIL\"\n" +
                                                    "         \"codigo\": 1233\n" +
                                                    "      },\n" +
                                                    "            {\n" +
                                                    "         \"key\": \"CONSULTAS\",\n" +
                                                    "         \"value\": \"Dr. Diego Andre\"\n" +
                                                    "         \"codigo\": 1234\n" +
                                                    "      },\n" +
                                                    "            {\n" +
                                                    "         \"key\": \"DEFAULT\",\n" +
                                                    "         \"value\": \"MI VALOR\"\n" +
                                                    "         \"codigo\": 1235\n" +
                                                    "      },\n" +
                                                    "   ]\n" +
                                                    "}";
} 

