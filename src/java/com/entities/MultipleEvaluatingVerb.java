/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.entities;

/**
 *
 * @author diegoarambulo
 */
//solo aplica para preguntas de multiple opcion, con la bandera de retroalimentacion abierta
public class MultipleEvaluatingVerb {
    
    private int idRule;
    private int idQuestion;
    private int minRange;
    private int maxRange;
    private String comentary;
    private String state;
    
    public MultipleEvaluatingVerb(){
        
    }

    public MultipleEvaluatingVerb(int idRule, int idQuestion, int minRange, int maxRange, String comentary, String state) {
        this.idRule = idRule;
        this.idQuestion = idQuestion;
        this.minRange = minRange;
        this.maxRange = maxRange;
        this.comentary = comentary;
        this.state = state;
    }
    
    public int getIdRule() {
        return idRule;
    }

    public int getIdQuestion() {
        return idQuestion;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setIdQuestion(int idQuestion) {
        this.idQuestion = idQuestion;
    }

    public void setIdRule(int idRule) {
        this.idRule = idRule;
    }

    public int getMinRange() {
        return minRange;
    }

    public void setMinRange(int minRange) {
        this.minRange = minRange;
    }

    public int getMaxRange() {
        return maxRange;
    }

    public void setMaxRange(int maxRange) {
        this.maxRange = maxRange;
    }

    public String getComentary() {
        return comentary;
    }

    public void setComentary(String comentary) {
        this.comentary = comentary;
    }
    
    public void reset(){
        comentary = "";
        minRange = 0;
        maxRange = 0;
    }
    
}
