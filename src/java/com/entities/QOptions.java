package com.entities;
/**
 *
 * @author DiegoAndre
 */
public class QOptions {
    
    private String qo_state;
    private String qo_sticker;
    private float qo_likert;
    private int qo_order;
    private int qp_idOption;
    private int qp_idQuestion;
    
    public QOptions(String sta,String stick,float like,int ord,int idop,int idq){
        this.qo_state = sta;
        this.qo_sticker = stick;
        this.qo_likert = like;
        this.qo_order = ord;
        this.qp_idOption = idop;
        this.qp_idQuestion = idq;
    }

    public String getQo_state() {
        return qo_state;
    }

    public void setQo_state(String qo_state) {
        this.qo_state = qo_state;
    }

    public String getQo_sticker() {
        return qo_sticker;
    }

    public void setQo_sticker(String qo_sticker) {
        this.qo_sticker = qo_sticker;
    }

    public float getQo_likert() {
        return qo_likert;
    }

    public void setQo_likert(float qo_likert) {
        this.qo_likert = qo_likert;
    }

    public int getQo_order() {
        return qo_order;
    }

    public void setQo_order(int qo_order) {
        this.qo_order = qo_order;
    }

    public int getQp_idOption() {
        return qp_idOption;
    }

    public void setQp_idOption(int qp_idOption) {
        this.qp_idOption = qp_idOption;
    }

    public int getQp_idQuestion() {
        return qp_idQuestion;
    }

    public void setQp_idQuestion(int qp_idQuestion) {
        this.qp_idQuestion = qp_idQuestion;
    }
    
    
}
