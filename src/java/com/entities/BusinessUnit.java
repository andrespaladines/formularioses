package com.entities;
/**
 *
 * @author Administrador
 */
public class BusinessUnit {
    private int code;
    private String name;
    private String iniciales;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIniciales() {
        return iniciales;
    }

    public void setIniciales(String iniciales) {
        this.iniciales = iniciales;
    }
    
    
}
