
package com.entities;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author DiegoAndre
 */
public class Question {
    
    private BigInteger p_questionId;
    private BigInteger s_idSection;
    private String p_name ="";
    private int p_order;
    private String p_type = "A";
    private boolean p_Questionstate = false;
        //si es abierta
        private String p_openAnswer ="";

        //si es multiple opcion
        private List<MultipleChoiceUnit> p_choicesList;
        //y si tiene opcion de retroalimentacion
        private List<MultipleEvaluatingVerb> p_evaluateList;
        //multiple opcion modo encuesta
        private List<QOptions> p_optionsList;
        
        //variables para recepcion de callback
        private String p_ruleMessage;
        private boolean p_chossen = false;
        
        //si es SI/NO
        private boolean p_booleanAnswer = false;
        
        //importante, este es el campo que recolecta la opcion escogida como respuesta
        private BigInteger p_idOptionAnswer;
        
        //mas importante, este campo captura las respuestas en caso de tener multiples entradas
        private BigInteger[] p_ListIdOptionAnswer;
        
    //define si tabula o no la pregunta
    private boolean p_registryValues = false;
    private boolean p_mandatory = false;
    private boolean p_callback = false;
    //2018-11-10 DAV - IPN veris
    private String tipoArea;
    //2019-02-20 DAV - IPN veris, codigo de servicio por pregunta
    private String internalCode;
    
    public Question(){
        this.setP_questionId(new BigInteger(Long.valueOf((new Date().getTime())).toString())); 
        p_choicesList = new ArrayList();
        p_optionsList = new ArrayList();
        p_evaluateList = new ArrayList();
    }
    
    public void reset(){
        p_name = "";
        p_order = 0;
        p_type = "A";
        p_openAnswer = "";
        p_choicesList.clear();
        p_evaluateList.clear();
        p_booleanAnswer = false;
        p_registryValues = false;
        p_booleanAnswer = false;
    }
    
    public String getOptionDescription(int id_option){
        String tmp_descrip ="";
        for(QOptions op : p_optionsList){
            if(op.getQp_idOption() == id_option){
                tmp_descrip = op.getQo_sticker();
                break;
            }
        }
        return tmp_descrip;
    }
    
    public float getOptionLikert(int id_option){
        float tmp_liker =0;
        for(QOptions op : p_optionsList){
            if(op.getQp_idOption() == id_option){
                tmp_liker = op.getQo_likert();
                break;
            }
        }
        return tmp_liker;
    }
    
    public void addNode(MultipleChoiceUnit unit){
        int ind = getIndexQuestion(unit);
        if(ind > -1){
           p_choicesList.set(ind, unit);
        }else{
           p_choicesList.add(unit);
        }  
    }
    
    public void addRule(MultipleEvaluatingVerb verb){
        int ind = getIndexEval(verb);
        if(ind > -1){
           p_evaluateList.set(ind, verb);
        }else{
           p_evaluateList.add(verb);
        }  
    }
    
    //metodo que detecta cambio en la respuesta multiple y dispara el mensaje necesitado para callback
    public void handleOptionChange(){  
        if(p_callback){
            p_ruleMessage = "";
            p_openAnswer = "";
            p_chossen = false;
            int orderValue = 0;
            boolean finIt = false;
            for(QOptions mcu : p_optionsList){
                if(mcu.getQp_idOption() == p_idOptionAnswer.intValue()){
                    orderValue = mcu.getQo_order();
                    finIt = true;
                    break;
                }
            } 
            if(finIt){
                for(MultipleEvaluatingVerb mev : p_evaluateList){
                    if(orderValue >= mev.getMinRange() && orderValue <= mev.getMaxRange()){
                        p_ruleMessage = mev.getComentary();
                        p_chossen = true;
                        break;
                    }
                }
            }
        }
    }
    
    public int getIndexEval(MultipleEvaluatingVerb unit){
        int index = -1;
        for(int ind = 0; ind < p_evaluateList.size();ind++){
            if(p_evaluateList.get(ind).getMinRange() == unit.getMinRange() && p_evaluateList.get(ind).getMaxRange() == unit.getMaxRange()){
                index= ind;
                break;
            }
        }
        return index;
    }
    
    public int getIndexQuestion(MultipleChoiceUnit unit){
        int index = -1;
        for(int ind = 0; ind < p_choicesList.size();ind++){
            if(p_choicesList.get(ind).getMc_index() == unit.getMc_index()){
                index= ind;
                break;
            }
        }
        return index;
    }
    
    public void deleteNode(int ind){
        for(int c=0;c<=p_choicesList.size();c++){
            if(p_choicesList.get(c).getMc_index().intValue()==ind){
                p_choicesList.remove(c);
                break;
            }
        }
    }
    
    public void deleteEvaluate(int min,int max){
        for(int c=0;c<=p_evaluateList.size();c++){
            if(p_evaluateList.get(c).getMinRange()==min && p_evaluateList.get(c).getMaxRange() == max){
                p_evaluateList.remove(c);
                break;
            }
        }
    }
    
    public MultipleChoiceUnit getSpecificNode(int ind){
        MultipleChoiceUnit choiseUnit = null;
        for(MultipleChoiceUnit MC : p_choicesList){
            if(MC.getMc_index().intValue() == ind){
                choiseUnit = MC;
                break;
            }
        }
        return choiseUnit;
    }
    
    public MultipleEvaluatingVerb getSpecificRule(int min,int max){
        MultipleEvaluatingVerb ruleUnit = null;
        for(MultipleEvaluatingVerb MC : p_evaluateList){
            if(MC.getMinRange()==min && MC.getMaxRange() == max){
                ruleUnit = MC;
                break;
            }
        }
        return ruleUnit;
    }
    //---------===========getters and setters

    public String getInternalCode() {
        return internalCode;
    }

    public void setInternalCode(String internalCode) {
        this.internalCode = internalCode;
    }
    
    public String getTipoArea() {
        return tipoArea;
    }

    public void setTipoArea(String tipoArea) {    
        this.tipoArea = tipoArea;
    }

    public BigInteger[] getP_ListIdOptionAnswer() {
        return p_ListIdOptionAnswer;
    }

    public void setP_ListIdOptionAnswer(BigInteger[] p_ListIdOptionAnswer) {
        this.p_ListIdOptionAnswer = p_ListIdOptionAnswer;
    }
    
    public boolean isP_state() {
        return p_Questionstate;
    }

    public BigInteger getP_idOptionAnswer() {
        return p_idOptionAnswer;
    }

    public void setP_idOptionAnswer(BigInteger p_idOptionAnswer) {
        this.p_idOptionAnswer = p_idOptionAnswer;
    }

    public void setP_state(boolean p_Questionstate) {
        this.p_Questionstate = p_Questionstate;
    }
    
    public BigInteger getS_idSection() {
        return s_idSection;
    }

    public void setS_idSection(BigInteger s_idSection) {
        this.s_idSection = s_idSection;
    }
    
    public BigInteger getP_questionId() {
        return p_questionId;
    }

    public void setP_questionId(BigInteger p_questionId) {
        this.p_questionId = p_questionId;
    }

    public String getP_name() {
        return p_name;
    }

    public String getP_ruleMessage() {
        return p_ruleMessage;
    }

    public void setP_ruleMessage(String p_ruleMessage) {
        this.p_ruleMessage = p_ruleMessage;
    }

    public void setP_name(String p_name) {
        this.p_name = p_name;
    }

    public int getP_order() {
        return p_order;
    }

    public void setP_order(int p_order) {
        this.p_order = p_order;
    }

    public String getP_type() {
        return p_type;
    }

    public boolean isP_chossen() {
        return p_chossen;
    }

    public void setP_chossen(boolean p_chossen) {
        this.p_chossen = p_chossen;
    }

    public void setP_type(String p_type) {
        this.p_type = p_type;
    }

    public String getP_openAnswer() {
        return p_openAnswer;
    }

    public void setP_openAnswer(String p_openAnswer) {
        this.p_openAnswer = p_openAnswer;
    }

    public List<MultipleChoiceUnit> getP_choicesList() {
        return p_choicesList;
    }

    public void setP_choicesList(List<MultipleChoiceUnit> p_choicesList) {
        this.p_choicesList = p_choicesList;
    }

    public boolean isP_booleanAnswer() {
        return p_booleanAnswer;
    }

    public void setP_booleanAnswer(boolean p_booleanAnswer) {
        this.p_booleanAnswer = p_booleanAnswer;
    }

    public boolean isP_registryValues() {
        return p_registryValues;
    }

    public void setP_registryValues(boolean p_registryValues) {
        this.p_registryValues = p_registryValues;
    }

    public boolean isP_mandatory() {
        return p_mandatory;
    }

    public void setP_mandatory(boolean p_mandatory) {
        this.p_mandatory = p_mandatory;
    }
    
    public boolean isP_callback() {
     return p_callback;
    }

    public void setP_callback(boolean p_callback) {
        this.p_callback = p_callback;
    }

    public List<QOptions> getP_optionsList() {
        return p_optionsList;
    }

    public void setP_optionsList(List<QOptions> p_optionsList) {
        this.p_optionsList = p_optionsList;
    }
    
    public List<MultipleEvaluatingVerb> getP_evaluateList() {
        return p_evaluateList;
    }

    public void setP_evaluateList(List<MultipleEvaluatingVerb> p_evaluateList) {
        this.p_evaluateList = p_evaluateList;
    }
    
    @Override
    public String toString(){
        String values="";
        StringBuilder strb=null;
            try{
                strb = new StringBuilder();
                strb.append("p_questionId: "+p_questionId+"\n");
                strb.append("p_name: "+p_name+"\n");
                strb.append("p_order: "+p_order+"\n");
                strb.append("p_type: "+p_type+"\n");
                strb.append("p_openAnswer: "+p_openAnswer+"\n");
                strb.append("p_choicesList: "+p_choicesList.size()+"\n");
                strb.append("p_evaluateList: "+p_evaluateList.size()+"\n");
                strb.append("p_booleanAnswer: "+p_booleanAnswer+"\n");
                strb.append("p_registryValues: "+p_registryValues+"\n");
                strb.append("p_mandatory: "+p_mandatory+"\n");
                
                values = strb.toString();
            }catch(Exception ex){
                values = ex.toString();
            }
        return values;
    }
    
}
