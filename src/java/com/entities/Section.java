package com.entities;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author DiegoAndre
 */
public class Section {
    private BigInteger s_idSection;
    private BigInteger s_idSurvey;
    private BigInteger s_idIndex;
    private int s_order;
    private String s_title;
    private String s_description;
    private int s_numberSection;
    private String s_state;
    private List<Question> sl_questions;
    
    @Override
    public String toString(){
        StringBuilder builder = null;
        String response = "";
        try{
            builder =  new StringBuilder();
            builder.append("\n");
            builder.append("\ns_idSection: ").append(s_idSection);
            builder.append("\ns_idSurvey: ").append(s_idSurvey);
            builder.append("\ns_title: ").append(s_title);
            builder.append("\ns_description: ").append(s_description);
            builder.append("\ns_numberSection: ").append(s_numberSection);
            builder.append("\ns_state: ").append(s_state);
            builder.append("\nsl_questions: --> ");
            for(Question quest : sl_questions){
                builder.append("\np_questionId: ").append(quest.getP_questionId());
                builder.append("\np_name: ").append(quest.getP_name());
                builder.append("\np_typed: ").append(quest.getP_type());
                builder.append("\np_numerOfOptions: ").append(quest.getP_optionsList().size());
            }
            
        }catch(Exception ex){
            ex.printStackTrace(System.err);
        }finally{
            response = builder.toString();
        }
        return response;
    }
    
    public Section(){
        sl_questions = new ArrayList();
        
    }

    public Section(BigInteger idSurvey,int order,String title,int numberSection,String state){
        BigInteger tmp = new BigInteger(String.valueOf((int)(Math.random()*(1024-1))+1));
        this.s_idSection = (tmp.add(new BigInteger(Long.valueOf((new Date().getTime())).toString()))); 
        this.s_idSurvey = idSurvey;
        this.s_order = order;
        this.s_title = title;
        this.s_numberSection = numberSection;
        this.s_state = state;
        sl_questions = new ArrayList();    
    }

    public String getS_description() {
        return s_description;
    }

    public void setS_description(String s_description) {
        this.s_description = s_description;
    }

    public BigInteger getS_idIndex() {
        return s_idIndex;
    }

    public void setS_idIndex(BigInteger s_idIndex) {
        this.s_idIndex = s_idIndex;
    }
    
    public List<Question> getSl_questions() {
        return sl_questions;
    }

    public void setSl_questions(List<Question> sl_questions){
        this.sl_questions = sl_questions;
    }
    
    public BigInteger getS_idSection() {
        return s_idSection;
    }

    public void setS_idSection(BigInteger s_idSection) {
        this.s_idSection = s_idSection;
    }

    public BigInteger getS_idSurvey() {
        return s_idSurvey;
    }

    public void setS_idSurvey(BigInteger s_idSurvey) {
        this.s_idSurvey = s_idSurvey;
    }

    public int getS_order() {
        return s_order;
    }

    public void setS_order(int s_order) {
        this.s_order = s_order;
    }

    public String getS_title() {
        return s_title;
    }

    public void setS_title(String s_title) {
        this.s_title = s_title;
    }

    public int getS_numberSection() {
        return s_numberSection;
    }

    public void setS_numberSection(int s_numberSection) {
        this.s_numberSection = s_numberSection;
    }

    public String getS_state() {
        return s_state;
    }

    public void setS_state(String s_state) {
        this.s_state = s_state;
    }
    
    
}
