package com.entities;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

/**
 *
 * @author DiegoAndre
 */
//solo aplica para preguntas de multiple opcion
public class MultipleChoiceUnit implements Comparable<MultipleChoiceUnit>{
    private BigInteger mc_index;
    private BigInteger mc_idQuestion;
    private String mc_description;
    private BigDecimal mc_liker;
    private int mc_order;
    private boolean p_ChoiceState = false;

    public MultipleChoiceUnit(){
        setMc_index(new BigInteger(Long.valueOf((new Date().getTime())).toString()));
        mc_liker = new BigDecimal(0);
    }
    
    public MultipleChoiceUnit(BigInteger index){
        mc_index = index;
        mc_description = "blank";
        mc_liker = new BigDecimal(0);
        mc_order = 1;
    }
    
    public void reset(){
        mc_description = "";
        mc_liker = new BigDecimal(0);
        mc_order = 1;
    }

    public BigInteger getMc_idQuestion() {
        return mc_idQuestion;
    }

    public void setMc_idQuestion(BigInteger mc_idQuestion) {
        this.mc_idQuestion = mc_idQuestion;
    }

    public boolean isP_ChoiceState() {
        return p_ChoiceState;
    }

    public void setP_ChoiceState(boolean p_ChoiceState) {
        this.p_ChoiceState = p_ChoiceState;
    }
    
    public BigInteger getMc_index() {
        return mc_index;
    }

    public void setMc_index(BigInteger mc_index) {
        this.mc_index = mc_index;
    }

    public String getMc_description() {
        return mc_description;
    }

    public void setMc_description(String mc_description) {
        this.mc_description = mc_description;
    }

    public BigDecimal getMc_liker() {
        return mc_liker;
    }

    public void setMc_liker(BigDecimal mc_liker) {
        this.mc_liker = mc_liker;
    }

    public int getMc_order() {
        return mc_order;
    }

    public void setMc_order(int mc_order) {
        this.mc_order = mc_order;
    }
    
    @Override
    public int compareTo(MultipleChoiceUnit t) {
        if(this.mc_order < t.mc_order){
            return -1;
        }else{
            return 1;
        }
    }
}
