package com.dao;

import com.utils.ConstantsSES;
import com.utils.Tools;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
/**
 *
 * @author Administrador
 */
public class ParticipantsDAO extends Tools{
    
    public int saveParticipant(HashMap<String,String> ph_participantInfo) throws SQLException{
        boolean resp = false;
        int tmp_idParticipant = 0;
        int results = 0;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        String table_name = "ses_participante";
        try{
           connection = (new Conexion().getConexion());
           preparedStatement = connection.prepareStatement(ConstantsSES.QUERY_INSERT_PARTICIPANTS);
           tmp_idParticipant = getNextIdParticipant();
           preparedStatement.setInt(1, tmp_idParticipant);
           preparedStatement.setInt(2, Integer.parseInt(ph_participantInfo.get("encuesta")));
           preparedStatement.setString(3, ph_participantInfo.get("ci"));
           preparedStatement.setString(4,ph_participantInfo.get("ci"));
           preparedStatement.setString(5,ph_participantInfo.get("nombre"));
           preparedStatement.setString(6,"A");
           preparedStatement.setString(7,"Ingresado");
           //2018-11-11 DAV - IPN veris
           preparedStatement.setString(8,ph_participantInfo.get("idIpnSensorEvento"));
           preparedStatement.setString(9,ph_participantInfo.get("fecha"));
           results = preparedStatement.executeUpdate();
           
        }catch(Exception ex){
            ex.printStackTrace(System.err);
        }finally{
            if(connection != null){
                connection.close();
            }
            if(preparedStatement != null){
                preparedStatement.close();
            }
        }
        return tmp_idParticipant;
    }
    
}
