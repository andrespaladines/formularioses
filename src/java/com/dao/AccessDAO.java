package com.dao;

import com.entities.BusinessUnit;
import com.utils.ConstantsSES;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrador
 */
public class AccessDAO {
    
    public List<BusinessUnit> getAllBusinessUnits() throws SQLException{
        List<BusinessUnit> tmp_units = null;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultset = null;
        BusinessUnit businessUnit = null;
        try{ 
           tmp_units = new ArrayList();
           connection = (new Conexion().getConexion());
           preparedStatement = connection.prepareStatement(ConstantsSES.QUERY_SELECT_BU);
           resultset = preparedStatement.executeQuery();
           while(resultset.next()){
               businessUnit = new BusinessUnit();
               businessUnit.setCode(resultset.getInt(1));
               businessUnit.setName(resultset.getString(2));
               businessUnit.setIniciales(resultset.getString(4));
               tmp_units.add(businessUnit);
           }
            
        }catch(Exception ex){
            ex.printStackTrace(System.err);
        }finally{
            if(connection != null){
                connection.close();
            }
            if(preparedStatement != null){
                preparedStatement.close();
            }
        }
            return tmp_units;
        }
    
    
}
