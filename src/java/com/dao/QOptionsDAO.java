package com.dao;

import com.entities.Question;
import com.entities.Section;
import com.utils.ConstantsSES;
import com.utils.Tools;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
/**
 *
 * @author Administrador
 */
public class QOptionsDAO extends Tools{
    
    public int saveToBase(String table_name,int idParticipante,int idQuestion, int idOptionAnswer,String openAnswer,Question quest,String urlFecha,String idCnm) throws SQLException{
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        int tmp_resp = 0;
        try{
        connection = (new Conexion().getConexion());
        preparedStatement = connection.prepareStatement(ConstantsSES.QUERY_INSERT_ANSWERS);
        preparedStatement.setInt(1, getNextIdResponse());
        preparedStatement.setInt(2,idParticipante); //TODO: preguntar que valor del participante va
        preparedStatement.setInt(3,idQuestion);
        if("M".equals(quest.getP_type()) || "SN".equals(quest.getP_type())|| "MR".equals(quest.getP_type())){
            //if(quest.getP_idOptionAnswer() != null){
                preparedStatement.setInt(4,idOptionAnswer);
            //}else{
            //    preparedStatement.setInt(4,0);
            //}
            preparedStatement.setString(5,openAnswer);
        }else{
            //preparedStatement.setInt(4,0);
            preparedStatement.setNull(4, java.sql.Types.INTEGER);
            preparedStatement.setString(5,openAnswer);
        }
        int codigo = 0;
        try{
            codigo = Integer.parseInt(quest.getInternalCode());
        }catch(Exception ex){
            //TODO: no error mostrado
        }
        preparedStatement.setInt(6,codigo);
        preparedStatement.setString(7,urlFecha);
        preparedStatement.setString(8,idCnm);
        tmp_resp = preparedStatement.executeUpdate();
        }catch(Exception ex){
            ex.printStackTrace(System.err);
        }finally{
            if(connection != null){
                connection.close();
            }
            if(preparedStatement != null){
                preparedStatement.close();
            }
        }
        return tmp_resp;
    }
    
    public boolean saveAnswerToBase(List<Section> pl_sections,int id_participante,int id_busUnit,String s_tittle,String urlFecha,String idCnm) throws SQLException{
        boolean resp = false;
        int error_counter = 0;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        String table_name = "ses_respuesta";
        int tmp_resp = 0;
        BigInteger[] p_ListIdOptionAnswer = null;
        
        for(Section sec : pl_sections){
            for(Question quest : sec.getSl_questions()){
                try{
                    if("MR".equals(quest.getP_type())){
                        p_ListIdOptionAnswer = quest.getP_ListIdOptionAnswer();
                        for(BigInteger Bi : p_ListIdOptionAnswer){                        
                            tmp_resp = saveToBase(table_name,id_participante,quest.getP_questionId().intValue(),Bi.intValue(),"n/a",quest,urlFecha,idCnm);
                            //------ingreso de consolidado
                            if(tmp_resp >= 1){
                              if(quest.isP_mandatory()&&("M".equals(quest.getP_type()) || "SN".equals(quest.getP_type())|| "MR".equals(quest.getP_type()))){
                                  consolida(id_busUnit,sec.getS_idSurvey().intValue(),s_tittle,quest.getP_questionId().intValue(),
                                    quest.getP_name(),
                                    Bi.intValue(),
                                    quest.getOptionDescription(Bi.intValue()),
                                    quest.getOptionLikert(Bi.intValue()));
                               }  
                            }
                        }
                    }else{
                        String openAnswerTMP = "";
                        int questionID = 0;
                        int optionAnswer = 0;
                        if("M".equals(quest.getP_type()) || "SN".equals(quest.getP_type())){
                            if(quest.isP_callback()){
                                openAnswerTMP = quest.getP_openAnswer();
                            }else{
                                openAnswerTMP = "n/a"; 
                            }
                            optionAnswer = quest.getP_idOptionAnswer().intValue();
                        }else{
                            openAnswerTMP = quest.getP_openAnswer();
                        }
                        questionID = quest.getP_questionId().intValue();
                        tmp_resp = saveToBase(table_name,id_participante,questionID,optionAnswer,openAnswerTMP,quest,urlFecha,idCnm);
                          if(tmp_resp >= 1){
                            if(quest.isP_mandatory()&&("M".equals(quest.getP_type()) || "SN".equals(quest.getP_type())|| "MR".equals(quest.getP_type()))){
                                consolida(id_busUnit,sec.getS_idSurvey().intValue(),s_tittle,quest.getP_questionId().intValue(),
                                      quest.getP_name(),quest.getP_idOptionAnswer().intValue(),
                                      quest.getOptionDescription(quest.getP_idOptionAnswer().intValue()),
                                      quest.getOptionLikert(quest.getP_idOptionAnswer().intValue()));
                             }  
                          }
                    }
                    //------------TODO: debe obtenerse los valores quemados
                }catch(Exception ex){
                    ex.printStackTrace(System.err);
                    error_counter++;
                }finally{
                    if(connection != null){
                        connection.close();
                    }
                    if(preparedStatement != null){
                        preparedStatement.close();
                    }
                }
            }
        }
        if(error_counter <= 0){
           resp = true; 
        }
        return resp;
    }
   
    
    public void consolida(int un_negocio,int id_encuesta,String nom_encuesta,int id_pregunta,String nom_pregunta,int id_opcion,String nom_opcion, float likert) throws SQLException{
        
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        String table_name = "ses_consolidado";
        int id_consolidado =0,cantidad = 0,id_actual = 0;
        try{
          connection = (new Conexion().getConexion());
          preparedStatement = connection.prepareStatement(ConstantsSES.QUERY_RESPONSE_EXIST);
          preparedStatement.setInt(1,un_negocio);
          preparedStatement.setInt(2,id_encuesta);
          preparedStatement.setString(3, nom_encuesta);
          preparedStatement.setInt(4,id_pregunta);
          preparedStatement.setInt(5,id_opcion);
          resultSet = preparedStatement.executeQuery();
          while(resultSet.next()){
              id_consolidado = resultSet.getInt(1);
              cantidad = resultSet.getInt(2);
              id_actual = resultSet.getInt(3);
              if(id_consolidado <= 0){ //no existe y debe ingresarse
                  preparedStatement = connection.prepareStatement(ConstantsSES.INSERT_INCOMING_RESPONSE);
                  preparedStatement.setInt(1,getNextId(table_name));
                  preparedStatement.setInt(2,un_negocio);
                  preparedStatement.setInt(3,id_encuesta);
                  preparedStatement.setString(4, nom_encuesta);
                  preparedStatement.setInt(5,id_pregunta);
                  preparedStatement.setString(6, nom_pregunta);
                  preparedStatement.setInt(7,id_opcion);
                  preparedStatement.setString(8, nom_opcion);
                  preparedStatement.setInt(9,1); //nace con 1
                  preparedStatement.setFloat(10, 0);
                  preparedStatement.setFloat(11, likert);
                  preparedStatement.setFloat(12, 0);
                  preparedStatement.executeUpdate();
              }else{//existe y debe actualizarse
                    preparedStatement = connection.prepareStatement(ConstantsSES.UPDATE_INCOMING_RESPONSE);
                    id_actual++;
                    preparedStatement.setInt(1,id_actual);
                    preparedStatement.setInt(2,id_consolidado);
                    int tmp = preparedStatement.executeUpdate();
              }
          }
        }catch(Exception ex){
            ex.printStackTrace(System.err);
        }finally{
            if(connection != null){
                connection.close();
            }
            if(preparedStatement != null){
                preparedStatement.close();
            }
        }
    }
}
