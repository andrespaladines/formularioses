package com.dao;

import com.entities.MultipleEvaluatingVerb;
import com.entities.QOptions;
import com.entities.Question;
import com.entities.Section;
import com.utils.ConstantsSES;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
/**
 *
 * @author DiegoAndre
 */
public class SurveyMasterDAO {
    
    String ls_tittle = "";
    String ls_message = "";
    String ls_imgHeader = "";
    String ls_imgMeditor = "";
    String ls_imgFoot = "";
    String ls_note = "";
    
    public boolean allReadyGetThisSurvey(String identification,int id_survey,String urlFecha) throws SQLException{
        boolean resp = true;
        int tmp_resp = 0;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try{
           connection = (new Conexion().getConexion());
           preparedStatement = connection.prepareStatement(ConstantsSES.QUERY_ALLREADY_SURVEY);
           preparedStatement.setInt(1,id_survey);
           preparedStatement.setString(2, identification);
           preparedStatement.setString(3,urlFecha);
           resultSet = preparedStatement.executeQuery();
           while(resultSet.next()){
               tmp_resp = resultSet.getInt(1);
           }
           if(tmp_resp <= 0){
               resp = false;
           }
        }catch(Exception ex){
            ex.printStackTrace(System.err);
        }finally{
            if(connection != null){
                connection.close();
            }
            if(preparedStatement != null){
                preparedStatement.close();
            }
            if(resultSet != null){
                resultSet.close();
            }
        }
        return resp;
    }
    
    
    public void getActiveSurveyById(int idSurvey) throws SQLException{
        int tmp_survey=0;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        System.err.println("OKDOAKSODAOS "+idSurvey);
        try{
           connection = (new Conexion().getConexion());
           preparedStatement = connection.prepareStatement(ConstantsSES.QUERY_VIEW_SURVEYS);
           preparedStatement.setInt(1,idSurvey);
           //titulo
           //mensaje
           resultSet = preparedStatement.executeQuery();
           while(resultSet.next()){
//            tmp_survey =  resultSet.getInt(1);
               System.out.println("confimado");
            
               System.out.println("ls_tittle "+ls_tittle);
            ls_message = resultSet.getString("mensaje");
            ls_imgHeader = resultSet.getString("imagen_cabecera");
            ls_imgMeditor = resultSet.getString("imagen_medicion");
            ls_imgFoot = resultSet.getString("imagen_pie");
            ls_note = resultSet.getString("nota_pie");
           }
           
        }catch(Exception ex){
            ex.printStackTrace(System.err);
        }finally{
            if(connection != null){
                connection.close();
            }
            if(preparedStatement != null){
                preparedStatement.close();
            }
            if(resultSet != null){
                resultSet.close();
            }
        }
//        return tmp_survey;
    }
    
    
    public void getActiveSurveyById2(int idSurvey) throws SQLException{
        int tmp_survey=0;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        System.err.println("OKDOAKSODAOS "+idSurvey);
        try{
           connection = (new Conexion().getConexion());
           preparedStatement = connection.prepareStatement(ConstantsSES.QUERY_VIEW_SURVEYS1);
           preparedStatement.setInt(1,idSurvey);
           //titulo
           //mensaje
           resultSet = preparedStatement.executeQuery();
           while(resultSet.next()){
//            tmp_survey =  resultSet.getInt(1);
               System.out.println("confimado");
            
               
            ls_tittle = resultSet.getString("TITULO");
            ls_message = resultSet.getString("mensaje");
            ls_imgHeader = resultSet.getString("imagen_cabecera");
            ls_imgMeditor = resultSet.getString("imagen_medicion");
            ls_imgFoot = resultSet.getString("imagen_pie");
            ls_note = resultSet.getString("nota_pie");
            System.out.println("ls_note "+ls_note);
           }
           
        }catch(Exception ex){
            ex.printStackTrace(System.err);
        }finally{
            if(connection != null){
                connection.close();
            }
            if(preparedStatement != null){
                preparedStatement.close();
            }
            if(resultSet != null){
                resultSet.close();
            }
        }
//        return tmp_survey;
    }
    
    public int getActiveSurveyFromBusinessUnit(int idBunit) throws SQLException{
        int tmp_survey=0;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        System.err.println("popopopopo");
        try{
           connection = (new Conexion().getConexion());
           preparedStatement = connection.prepareStatement(ConstantsSES.QUERY_VIEW_SURVEYS);
           preparedStatement.setInt(1,idBunit);
           //titulo
           //mensaje
           resultSet = preparedStatement.executeQuery();
           while(resultSet.next()){
            tmp_survey =  resultSet.getInt(1);
            ls_tittle = resultSet.getString("TITULO");
            ls_message = resultSet.getString("MENSAJE");
            ls_tittle = resultSet.getString("titulo");
               System.out.println("ls_tittle "+ls_tittle);
    
            ls_imgHeader = resultSet.getString("imagen_cabecera");
            ls_imgMeditor = resultSet.getString("imagen_medicion");
            ls_imgFoot = resultSet.getString("imagen_pie");
            ls_note = resultSet.getString("nota_pie");
           }
           
        }catch(Exception ex){
            ex.printStackTrace(System.err);
        }finally{
            if(connection != null){
                connection.close();
            }
            if(preparedStatement != null){
                preparedStatement.close();
            }
            if(resultSet != null){
                resultSet.close();
            }
        }
        return tmp_survey;
    }
    public List<Section> getAllSections(int idSurvey,HashMap<String,String>userData,HashMap<String,String[]>replacements)throws SQLException{
        List<Section> lsects_sections = null;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Section tmp_section = null;
        try{
           lsects_sections = new ArrayList();
           connection = (new Conexion().getConexion());
           preparedStatement = connection.prepareStatement(ConstantsSES.QUERY_VIEW_SECTIONS);
           preparedStatement.setInt(1,idSurvey);
           resultSet = preparedStatement.executeQuery(); 
           while(resultSet.next()){
               tmp_section = new Section();
               tmp_section.setS_idSection(new BigInteger(String.valueOf(resultSet.getInt(1))));
               tmp_section.setS_idSurvey(new BigInteger(String.valueOf(resultSet.getInt(2))));
               tmp_section.setS_idIndex(new BigInteger(String.valueOf(resultSet.getInt(3))));
               tmp_section.setS_title(resultSet.getString(4));
               tmp_section.setS_description(resultSet.getString(5));
               tmp_section.setS_numberSection(resultSet.getInt(6));
               tmp_section.setS_state(resultSet.getString(7));
               List<Question> questionList = getAllQuestionForSection(resultSet.getInt(1),userData,replacements);
               //2018-11-11 DAV - IPN veris
               //para evitar secciones vacias que se hayan depurado por tener preguntas no aptas para el cliente
               if(questionList.size() > 0){
                  tmp_section.setSl_questions(questionList);
                  lsects_sections.add(tmp_section); 
               }
           }
           
        }catch(Exception ex){
            ex.printStackTrace(System.err);
        }finally{
            if(connection != null){
                connection.close();
            }
            if(preparedStatement != null){
                preparedStatement.close();
            }
            if(resultSet != null){
                resultSet.close();
            }
        }
        return lsects_sections;
    }
    
    public List<Question> getAllQuestionForSection(int idSection,HashMap<String,String>userData,HashMap<String,String[]>replacements) throws SQLException{
        List<Question> lques_quesrions = null;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Question tmp_quest = null;
        StringTokenizer tokenizer = null;
        try{
           lques_quesrions = new ArrayList();
           connection = (new Conexion().getConexion());
           preparedStatement = connection.prepareStatement(ConstantsSES.QUERY_VIEW_QUESTIONS);
           preparedStatement.setInt(1,idSection);
           resultSet = preparedStatement.executeQuery(); 
           while(resultSet.next()){
               tmp_quest = makeQuestion(resultSet.getInt(1),
                                            resultSet.getInt(2),
                                            resultSet.getString(3),
                                            resultSet.getString(4),
                                            resultSet.getInt(5),
                                            resultSet.getString(6),
                                            resultSet.getString(7),
                                            resultSet.getString(8),
                                            resultSet.getString(9),//parametro de reglas
                                            resultSet.getString(10)); //parametro de tipoArea
                //2018-11-11 DAV - IPN veris
                boolean addQuestion = false;
                if(userData != null && userData.get("areas")!= null ){
                    tokenizer = new StringTokenizer(userData.get("areas"),"|");
                    while (tokenizer.hasMoreTokens()){
                        String tok = tokenizer.nextToken();
                        if(tok.equals(tmp_quest.getTipoArea()) || "DEFAULT".equals(tmp_quest.getTipoArea())){
                            addQuestion = true;
                            break;
                        }
                    }  
                }else{
                    if("DEFAULT".equals(tmp_quest.getTipoArea())){
                        addQuestion = true;
                    }
                }
                
                if(addQuestion){
                    if(!tmp_quest.getP_type().equals("A")){
                        List<QOptions> tmplist = getAllOptionsFromQuestion(resultSet.getInt(1));
                        tmp_quest.setP_optionsList(tmplist);
                        if(tmp_quest.getP_type().equals("M") && tmp_quest.isP_callback()){
                            //seteo de reglas
                            tmp_quest.setP_evaluateList(getAllRulesFromQuestion(resultSet.getInt(1)));
                        }
                    }
                    //mecanismo de remplazo para parametros dinamicos en preguntas
                    if(replacements != null ){
                       String contenido = tmp_quest.getP_name();
                       for(Map.Entry<String, String[]> entry : replacements.entrySet()) {
                           String[] dataValue = entry.getValue();
                           //DAV - 20022019 se agrega array de valores ([0]value,[1]codigo)
                           contenido = contenido.replaceAll("@"+entry.getKey()+"@", dataValue[0]);
                           try{
                               String internalCode = dataValue[1];
                              if(tmp_quest.getTipoArea().equals(entry.getKey())){
                                  tmp_quest.setInternalCode(internalCode);
                              } 
                           }catch(Exception ex){
                               ex.printStackTrace();
                           }
                       }
                       tmp_quest.setP_name(contenido);
                    }
                    lques_quesrions.add(tmp_quest); 
                }
                //
           }
        }catch(Exception ex){
            ex.printStackTrace(System.err);
        }finally{
            if(connection != null){
                connection.close();
            }
            if(preparedStatement != null){
                preparedStatement.close();
            }
            if(resultSet != null){
                resultSet.close();
            }
        }
        return lques_quesrions;
    }
    
    public List<QOptions> getAllOptionsFromQuestion(int isQuestion) throws SQLException{
        List<QOptions> tmp_qoptions = null;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try{
           tmp_qoptions = new ArrayList();
           connection = (new Conexion().getConexion());
           preparedStatement = connection.prepareStatement(ConstantsSES.QUERY_VIEW_OPTIONS);
           preparedStatement.setInt(1,isQuestion);
           resultSet = preparedStatement.executeQuery();
           while(resultSet.next()){
               tmp_qoptions.add(new QOptions(resultSet.getString(1),
                                            resultSet.getString(2),
                                            resultSet.getFloat(3),
                                            resultSet.getInt(4),
                                            resultSet.getInt(5),
                                            resultSet.getInt(6)));
           }
           
        }catch(Exception ex){
            ex.printStackTrace(System.err);
        }finally{
            if(connection != null){
                connection.close();
            }
            if(preparedStatement != null){
                preparedStatement.close();
            }
            if(resultSet != null){
                resultSet.close();
            }
        }
        
        return tmp_qoptions;
    }
    
    public List<MultipleEvaluatingVerb> getAllRulesFromQuestion(int idQuestion) throws SQLException{
        List<MultipleEvaluatingVerb> tmp_rules = null;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try{
           tmp_rules = new ArrayList();
           connection = (new Conexion().getConexion());
           preparedStatement = connection.prepareStatement(ConstantsSES.QUERY_VIEW_RULES);
           preparedStatement.setInt(1,idQuestion);
           resultSet = preparedStatement.executeQuery();
           while(resultSet.next()){
               tmp_rules.add(new MultipleEvaluatingVerb(resultSet.getInt(1),
                                            resultSet.getInt(2),
                                            resultSet.getInt(3),
                                            resultSet.getInt(4),
                                            resultSet.getString(5),
                                            resultSet.getString(6)
                                            ));
           }
           
        }catch(Exception ex){
            ex.printStackTrace(System.err);
        }finally{
            if(connection != null){
                connection.close();
            }
            if(preparedStatement != null){
                preparedStatement.close();
            }
            if(resultSet != null){
                resultSet.close();
            }
        }
        
        return tmp_rules;
    }
    
    public Question makeQuestion(int questid,int sectionid,String name,String mandatory,int order,String state,String registValues,String type,String rules,String tipoArea){
        Question tmp_quest = null;
        tmp_quest = new Question();
        tmp_quest.reset();
            tmp_quest.setP_questionId(new BigInteger(String.valueOf(questid)));
            tmp_quest.setS_idSection(new BigInteger(String.valueOf(sectionid)));
            tmp_quest.setP_name(name);
            tmp_quest.setP_mandatory(stringToBoolean(mandatory));
            tmp_quest.setP_order(order);
            tmp_quest.setP_state(stringToBoolean(state));
            tmp_quest.setP_registryValues(stringToBoolean(registValues));
            tmp_quest.setP_type(type);
            tmp_quest.setP_callback(stringToBoolean(rules));
            tmp_quest.setTipoArea(tipoArea);
        return tmp_quest;
    }
    
    public boolean stringToBoolean(String ls_field){
        if("S".equals(ls_field) || "A".equals(ls_field)){
            return true;
        }else{
            return false;
        }
    }

    public String getLs_tittle() {
        return ls_tittle;
    }

    public String getLs_message() {
        return ls_message;
    }

    public String getLs_imgHeader() {
        return ls_imgHeader;
    }

    public void setLs_imgHeader(String ls_imgHeader) {
        this.ls_imgHeader = ls_imgHeader;
    }

    public String getLs_imgMeditor() {
        return ls_imgMeditor;
    }

    public void setLs_imgMeditor(String ls_imgMeditor) {
        this.ls_imgMeditor = ls_imgMeditor;
    }

    public String getLs_imgFoot() {
        return ls_imgFoot;
    }

    public void setLs_imgFoot(String ls_imgFoot) {
        this.ls_imgFoot = ls_imgFoot;
    }

    public String getLs_note() {
        return ls_note;
    }

    public void setLs_note(String ls_note) {
        this.ls_note = ls_note;
    }
    
    
    
}
