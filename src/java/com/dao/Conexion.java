package com.dao;

import com.utils.ConstantsSES;
import java.sql.Connection;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

public class Conexion {
    
    private DataSource dataSource=null;
    private Context  initialContext =null;
    private String jndi=ConstantsSES.JNDI;
    private Connection conex=null;
    
    public Conexion(){
        try{
        initialContext = new InitialContext();
        dataSource = (DataSource)initialContext.lookup(jndi);
        initialContext.close();    
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
    
    public DataSource getDataSource(){
        return dataSource;
    }
    
    public Connection getConexion(){
        try{
            conex = dataSource.getConnection();
        }catch(Exception ex){
            
        }
        return conex;
    }
    
   
}
